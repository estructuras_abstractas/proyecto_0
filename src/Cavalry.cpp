/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Cavalry.cpp
 * @brief Se definen los métodos de la clase Cavalry.
 */

#include <iostream>
#include "../include/Cavalry.h"

using namespace std;


Cavalry::Cavalry()
{
    this->type            = 'C'; 
    this->maxHitPoints    = 25;
    this->hitPoints       = 25;
    this->attack          = 15;
    this->defense         = 5;
    this->range           = 1;
    this->level           = 1;
    this->experience      = 0;
    this->movement        = 3;
    this->cost            = 5;

    this->posX            = 0;
    this->posY            = 0;

    this->vive            = true;
}


bool Cavalry::move ( int posiX, int posiY, Field campo )
{   
    bool continuar = true;

    //< Verifica que las coordenadas estén en el rango de la matriz.
    if ( posiX > (this->posX+this->movement) || posiY > (this->posY+this->movement) ) return false;


    //< Se lleva a cabo el movimiento en cruz.
    if ( (this->posX < posiX) && (this->posY == posiY) )
    {    
        while( continuar == true )
        { 
            if ( this->posX == posiX )
            {
                continuar = false;   
            }

            else if (campo.posicion_playfield_valida ( this->posX+1, posiY ) )
            {
                campo.reset_cell( this->posX, this->posY );
                this->posX++;
                campo.set_army( this->type, this->id, this->posX, posiY );     
            }
                            
        }//Fin de while
    }
        
    else if ( (this->posX > posiX) && (this->posY == posiY) )
    {
        while ( continuar == true )
        {    
            if ( this->posX == posiX )
            {
                continuar = false;   
            }

            else if (campo.posicion_playfield_valida ( this->posX-1, posiY ) )
            {
                campo.reset_cell( this->posX, this->posY );
                this->posX--;
                campo.set_army( this->type, this->id, this->posX, posiY );   
            }
                            
        }//Fin de while
    }
        
    else if ( (this->posX == posiX) && (this->posY < posiY) )
    {
        while( continuar == true )
        { 
            if ( this->posY == posiY )
            {
                continuar = false;   
            }

            else if (campo.posicion_playfield_valida ( posiX, this->posY+1 ) )
            {
                campo.reset_cell( this->posX, this->posY );
                this->posY++;
                campo.set_army( this->type, this->id, posiX, this->posY );     
            }
                            
        }//Fin de while
    }
        
    else if ( (this->posX == posiX) && (this->posY > posiY) )
    {
        while( continuar == true )
        { 
            if ( this->posY == posiY )
            {
                continuar = false;   
            }

            else if (campo.posicion_playfield_valida ( posiX, this->posY-1 ) )
            {
                campo.reset_cell( this->posX, this->posY );
                this->posY--;
                campo.set_army( this->type, this->id, posiX, this->posY );     
            }
                            
        }//Fin de while
    }
        
    //< Se lleva a cabo el movimiento en diagonal.
    else if ( campo.posicion_playfield_valida ( posiX, posiY ) )
    {
        if ( 
            (this->posX-1 == posiX) && (this->posY-1 == posiY) ||
            (this->posX+1 == posiX) && (this->posY+1 == posiY) ||
            (this->posX-1 == posiX) && (this->posY+1 == posiY) ||
            (this->posX+1 == posiX) && (this->posY-1 == posiY)
        )
        {
            campo.reset_cell( this->posX, this->posY );
            this->posX = posiX;
            this->posY = posiY;
            campo.set_army( this->type, this->id, posiX, posiY );
        }

        else return false;
    }

    else return false;
}
