/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Unit.cpp
 * @brief Se definen los métodos de la clase Unit.
 */

#include <iostream>
#include "../include/Unit.h"

using namespace std;


void Unit::posicion_inicial ( int posiX, int posiY )
{
    this->posX = posiX;
    this->posY = posiY;
}


bool Unit::move ( int posiX, int posiY, Field campo )
{
    //< Verifica que las coordenadas son válidas, en ese caso se mueve la unidad.
    if ( campo.posicion_playfield_valida ( posiX, posiY ) )
    {
        if ( 
            (this->posX-1 == posiX) && (this->posY == posiY) ||
            (this->posX+1 == posiX) && (this->posY == posiY) ||
            (this->posX == posiX) && (this->posY-1 == posiY) ||
            (this->posX == posiX) && (this->posY+1 == posiY)
        )
        {
            campo.reset_cell( this->posX, this->posY );
            this->posX = posiX;
            this->posY = posiY;
            campo.set_army( this->type, this->id, posiX, posiY );
        }

        else return false;
        
    }//Fin de if

    else
    {
        return false;
    }  
}


bool Unit::atacar ( int posiX, int posiY, vector<Unit*> &enemigo, Field campo )
{
    int dano;

    //< Verifica que las coordenadas estén en el rango de la matriz.
    if ( posiX >= campo.get_rows() || posiY >= campo.get_cols() ) return false;


    if ( campo.hay_alguien (posiX, posiY) == true )
    {
        if ( 
            (this->posX-1 == posiX) && (this->posY == posiY) ||
            (this->posX+1 == posiX) && (this->posY == posiY) ||
            (this->posX == posiX) && (this->posY-1 == posiY) ||
            (this->posX == posiX) && (this->posY+1 == posiY)
        )
        {
            dano = this->attack - enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->get_defense();

            cout << endl << endl;
            cout << "==> "; 
            cout << enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->get_type() << " (" << campo.get_id_army_en_cell(posiX, posiY) << ")";
            cout << " recibió " << dano << " puntos de daño." << endl << endl;

            enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->set_hitPoints( dano );
            
            return true;
        }
        else return false;
    }
    else return false;
}


void Unit::imprimir_estado_unit ()
{
    cout << endl;

    if ( this->vive == true )
    {
        cout << "--> id:          " << this->id << endl;
        cout << "--> Tipo:        " << this->type << endl;
        cout << "--> HP:          " << this->maxHitPoints << "/" << this->hitPoints << endl;
        cout << "--> ataque:      " << this->attack << endl;
        cout << "--> defensa:     " << this->defense << endl;
        cout << "--> rango:       " << this->range << endl;
        cout << "--> nivel:       " << this->level << endl;
        cout << "--> experiencia: " << this->experience << endl;
    }

    else
    {
        cout << endl << "--> La unidad está muerta, se equivocó con el id o esa unidad es del enemigo :/" << endl;
    }
}


void Unit::subir_exp()
{
    this->experience++;
}


void Unit::subir_nivel()
{
    //< Verifica si la experiencia es el doble del nivel, en ese caso sube un nivel.
    if( this->experience >= 2*this->level )
    {
        cout << endl << endl << "**********************************" << endl;
        cout <<                 "    ¡" << this->type << " ("<< this->id << ") Subió un nivel!" << endl;
        cout <<                 "**********************************" << endl << endl;

        this->maxHitPoints = this->maxHitPoints + 0.25*(this->maxHitPoints);
        this->hitPoints    = this->maxHitPoints;
        this->attack       = this->attack + 0.25*this->attack;
        this->defense      = this->defense + 0.25*this->defense;
        this->level++;
        this->experience   = 0;
    }
}




int Unit::get_id()
{
    return this->id;
}


char Unit::get_type ()
{
    return this->type;
}


int Unit::get_maxHitPoints()
{
    return this->maxHitPoints;
}


int Unit::get_hitPoints()
{
    return this->hitPoints;
}


int Unit::get_attack()
{
    return this->attack;
}


int Unit::get_defense()
{
    return this->defense;
}
   


int Unit::get_range()
{
    return this->range;
}


int Unit::get_level()
{
    return this->level;
}


int Unit::get_experience()
{
    return this->experience;
}


int Unit::get_movement()
{
    return this->movement;
}



int Unit::get_posX()
{
    return this->posX;
}


int Unit::get_posY()
{
    return this->posY;
}



int Unit::get_cost()
{
    return this->cost;
}


int Unit::get_idcount()
{
    return this->idCount;
}


bool Unit::get_vive()
{
    return this->vive;
}


void Unit::set_id ( int id )
{
    this->id = id;
}


void Unit::set_type ( char tipo )
{
    this->type;
}


void Unit::set_maxHitpoints ( int max )
{
    this->maxHitPoints;
}


void Unit::set_hitPoints ( int quitar_vida )
{
    this->hitPoints = this->hitPoints - quitar_vida;

    //< Hace que el personaje no tenga vida negativa.
    if( this->hitPoints <= 0 )
        this->hitPoints = 0;
}


void Unit::set_hitPoints ( int hp, char ignorar )
{
    this->hitPoints = hp;
}


void Unit::set_attack ( int ataque )
{
    this->attack;
}


void Unit::set_defense ( int defensa )
{
    this->defense;
}


void Unit::set_range ( int rango )
{
    this->range;
}


void Unit::set_level ( int nivel )
{
    this->level;
}


void Unit::set_exp ( int exp )
{
    this->experience;
}


void Unit::set_movement ( int movimiento )
{
    this->movement;
}


void Unit::set_cost ( int costo )
{
    this->cost;
}


void Unit::set_idCount ( int idcount )
{
    this->idCount = idcount;
}


void Unit::set_vive ( bool vivo )
{
    this->vive = vivo;
}