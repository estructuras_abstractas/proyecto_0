/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Player.cpp
 * @brief Se definen los métodos de la clase Player.
 */

#include <iostream>
#include <stdio_ext.h>

#include "../include/Player.h"
#include "../include/Cavalry.h"
#include "../include/Lancer.h"
#include "../include/Archer.h"

#define DINERO 15

using namespace std;


Player::Player()
{
    this->maxCost = DINERO;
    this->score   = 0;
}


Player::~Player()
{
    this->army.clear();
}


void Player::posicion_inicial( Field campo, int jugador, int idcount )
{
        if ( jugador == 1 )
        {
            for ( int j=0; j<campo.get_cols(); j++ )   
            {
                for ( int i=0; i<campo.get_rows(); i++ )
                {
                    if ( campo.posicion_playfield_valida( i, j ) == true )
                    {
                        this->army [idcount]->posicion_inicial( i, j );
                        campo.set_army( this->army [idcount]->get_type(), this->army [idcount]->get_id(), i, j );

                        i = campo.get_rows();
                        j = campo.get_cols();
                    }
                   
                }//Fin de for
            }//Fin de for
        }
        else
        {
            for ( int j=0; j<campo.get_cols(); j++ )   
            {
                for ( int i=0; i<campo.get_rows(); i++ )
                {
                    if ( campo.posicion_playfield_valida( i, campo.get_cols() - 1 - j ) == true )
                    {
                        this->army [idcount]->posicion_inicial( i, campo.get_cols() - 1 - j );
                        campo.set_army( this->army [idcount]->get_type(), this->army [idcount]->get_id(), i, campo.get_cols() - 1 - j );

                        i = campo.get_rows();
                        j = campo.get_cols();
                    }
                   
                }//Fin de for
            }//Fin de for          
        }    
}


void Player::createArmy( Field campo, int jugador )
{
    bool continuar = true;
    int eleccion;
    int idcount = 0;

    cin.clear();
    __fpurge(stdin); ///< Limpia el buffer de cin.
    
    cout << endl;
    cout << "~~ Nombre del jugador: ";
    cin >> this->name;

    cout << endl;
    cout << "~~ Compre sus unidades, tiene " << this->maxCost << " Guiles como máximo ~~" << endl << endl;

    while ( continuar == true )
    {
        cin.clear();
        __fpurge(stdin); ///< Limpia el buffer de cin.

        cout << "1. Arquero" << endl;
        cout << "2. Lancer" << endl;
        cout << "3. caballero" << endl;
        cout << "4. Listo" << endl;
        cin >> eleccion;

         //< Se inserta el tipo de unidad especificada y se verifica si el jugador tiene el dinero para comprarla.   
        if ( eleccion == 1 )
        {
            this->army.push_back (new Archer());
            

            if ( this->army[idcount]->get_cost() > this->maxCost )
            {
                this->army.pop_back();
                cout << endl << "--> No hay plata :v" << endl << endl;
            }
            
            else 
            {
                cout << endl << "--> Arquero (" << idcount << ") invocado <--" << endl << endl;
                this->army[idcount]->set_id (idcount);
                this->maxCost = this->maxCost - this->army[idcount]->get_cost();
                posicion_inicial ( campo, jugador, idcount );
                idcount++;
            }
        } 
            
        else if ( eleccion == 2 )
        {
            this->army.push_back (new Lancer());

            if ( this->army[idcount]->get_cost() > this->maxCost )
            {
                this->army.pop_back();
                cout << endl << "--> No hay plata :v" << endl <<  endl;
            }

            else
            {
                cout << endl << "--> Lancero (" << idcount << ") invocado <--" << endl << endl;
                this->army[idcount]->set_id (idcount);
                this->maxCost = this->maxCost - this->army[idcount]->get_cost();
                posicion_inicial ( campo, jugador, idcount );
                idcount++;
            }
        }

        else if ( eleccion == 3 )
        {
            this->army.push_back (new Cavalry());

            if ( this->army[idcount]->get_cost() > this->maxCost )
            {
                this->army.pop_back();
                cout << endl << "--> No hay plata :v" << endl << endl;
            }

            else 
            {
                cout << endl << "--> Caballero (" << idcount << ") invocado <--" << endl << endl;
                this->army[idcount]->set_id (idcount);
                this->maxCost = this->maxCost - this->army[idcount]->get_cost();
                posicion_inicial ( campo, jugador, idcount );
                idcount++;
            }
        }

        else if ( eleccion == 4 )
        {
            if (this->army.size() != 0 )
                continuar = false;
            else cout << endl << "-> Escoja almenos una unidad -_-" << endl << endl;
        }

        else
        {
            cout << "-> Esa opción no existe :/" << endl << endl << endl;
        }
           
 
    }//Fin de while

    cout << endl;

    //< Todas las unidades llevan la cantidad de compañeros que tienen.
    for ( int i=0; i<this->army.size(); i++ )
        this->army[i]->set_idCount( idcount );
}


void Player::play( Field &campo, vector<Unit*> &enemigo )
{
    bool continuar = true;
    char moverse;
    char atacar;
    int posX;
    int posY;


    for ( int i=0; i<this->army.size(); i++ )
    {
        //< Se verifica si la unidad está viva.
        if( this->army[i]->get_vive() == true )
        {
            cout << endl;
            cout << " Unidad " << this->army[i]->get_type() << " (" << this->army[i]->get_id() << "):";
            cout << endl << endl;

            continuar = true;

            //mover
            while( continuar == true )
            {
                cin.clear();
                __fpurge(stdin);
                
                cout << "-> Moverse?: ";
                cin >> moverse;

                if( moverse == 's' )
                {
                    cout << endl;
                    cout << "---> coordenada?:" << endl << endl;
                    
                    cout << "         x: ";
                    cin >> posX;
                    
                    cout << "         y: ";
                    cin >> posY;

                    if ( posX == -1 && posY == -1 ) return;

                    if( this->army[i]->move( posX, posY, campo ) )
                    {
                        continuar = false;
                    }

                    else cout << endl << endl << "---> Posición inválida..." << endl;

                    campo.imprimir_estado_field(); 

                    cout << endl;
                }

                else if ( moverse == 'n' ) continuar = false;
                
                else cout << endl << "---> Escriba s para si, o n para no..." << endl << endl;
            }//Fin de while


            //atacar
            continuar = true;

            while( continuar == true )
            {
                //Atacar
                cin.clear();
                __fpurge(stdin);
                
                cout << endl << "-> Atacar?: ";
                cin >> atacar;

                if( atacar == 's' )
                {
                    cout << endl;
                    cout << "---> coordenada?:" << endl << endl;
                    
                    cout << "         x: ";
                    cin >> posX;
                    
                    cout << "         y: ";
                    cin >> posY;

                    if ( posX == -1 && posY == -1 ) return;


                    if( this->army[i]->atacar( posX, posY, enemigo, campo ) )
                    {
                        continuar = false;

                        if ( enemigo[ campo.get_id_army_en_cell(posX, posY) ]->get_hitPoints() <= 0 )
                        {

                            enemigo[ campo.get_id_army_en_cell(posX, posY) ]->set_vive (false);

                            for ( int k=0; k<enemigo.size(); k++)
                                enemigo[k]->set_idCount ( enemigo[k]->get_idcount() -1  );

                        
                            campo.reset_cell ( posX, posY );
                            this->army[i]->subir_exp();
                            this->army[i]->subir_nivel();
                            this->set_score ( this->score++ );
                        } 
                    }
                    
                    else cout << endl << "---> Ahí no se puede atacar ;o" << endl << endl;


                }

                else if ( atacar == 'n' ) continuar = false;
                
                else cout << endl << "---> Escriba s para si, o n para no..." << endl << endl;
        
        
            }//Fin de while
        
        }//Fin de if

    }//Fin de for
    
}//Fin de la funcion


void Player::actualiza_army ( vector<Unit*> nueva_army )
{
    this->army = nueva_army;
}


//Regresa los parámetros de player

int Player::get_maxCost()
{
    return this->maxCost;
}


string Player::get_name()
{
    return this->name;
}


vector <Unit*> Player::get_army()
{
    return this->army;
}


int Player::get_score()
{
    return this->score;
}


void Player::set_maxCost ( int maxcost )
{
    this->maxCost = maxcost;
}


void Player::set_name ( std::string nombre )
{
    this->name = nombre;
}


void Player::set_score ( int puntuacion )
{
    this->score = puntuacion;
}


void Player::imprimir_estado_unidad ()
{
    bool continuar = true;
    char conocer;
    int id;

    while ( continuar == true )
    {
        cin.clear();
        __fpurge(stdin);
        
        cout << endl << "Conocer estado de una unidad? ";
        cin >> conocer;

        if ( conocer == 's' )
        {
            cin.clear();
            __fpurge(stdin);    

            cout << endl << "--> Id: ";
            cin >> id;

            if ( id <= this->army.size()-1 )
            {
                this->army [id]->imprimir_estado_unit();
                //continuar = false;
            }
            else cout << endl << "--> Id incorrecta -_-" << endl;
        }

        else if ( conocer == 'n' )
        {
            continuar = false;
        }

        else cout << endl << "---> Escriba s para si, o n para no..." << endl;

    }//Fin de while
}