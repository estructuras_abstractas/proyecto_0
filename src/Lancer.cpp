/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Lancer.cpp
 * @brief Se definen los métodos de la clase Lancer.
 */

#include <iostream>
#include "../include/Lancer.h"

using namespace std;


Lancer::Lancer()
{
    this->type            = 'L'; 
    this->maxHitPoints    = 20;
    this->hitPoints       = 20;
    this->attack          = 7;
    this->defense         = 7;
    this->range           = 1;
    this->level           = 1;
    this->experience      = 0;
    this->movement        = 1;
    this->cost            = 3;

    this->posX            = 0;
    this->posY            = 0;

    this->vive            = true;
}
