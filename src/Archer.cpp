/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Archer.cpp
 * @brief Se definen los métodos de la clase Archer.
 */

#include <iostream>
#include "../include/Archer.h"

using namespace std;


Archer::Archer()
{
    this->type            = 'A'; 
    this->maxHitPoints    = 15;
    this->hitPoints       = 15;
    this->attack          = 9;
    this->defense         = 5;
    this->range           = 3;
    this->level           = 1;
    this->experience      = 0;
    this->movement        = 1;
    this->cost            = 4;

    this->posX            = 0;
    this->posY            = 0;

    this->vive            = true;
}


bool Archer::atacar ( int posiX, int posiY, std::vector<Unit*> &enemigo, Field campo )
{
    int dano;

    //< Verifica que las coordenadas estén en el rango de la matriz.
    if ( posiX >= campo.get_rows() || posiY >= campo.get_cols() ) return false;

    //< Si hay alguien en la posición verifica si esa posicón es válida.
    if ( campo.hay_alguien (posiX, posiY) == true )
    {
        if ( 
            (this->posX-3 == posiX) && (this->posY - 6 == posiY) ||
            (this->posX-3 == posiX) && (this->posY - 5 == posiY) ||
            (this->posX-3 == posiX) && (this->posY - 4 == posiY) ||
            (this->posX-3 == posiX) && (this->posY - 3 == posiY) ||
            (this->posX-3 == posiX) && (this->posY - 2 == posiY) ||
            (this->posX-3 == posiX) && (this->posY - 1 == posiY) ||
            (this->posX-3 == posiX) && (this->posY     == posiY) ||

            (this->posX+3 == posiX) && (this->posY - 6 == posiY) ||
            (this->posX+3 == posiX) && (this->posY - 5 == posiY) ||
            (this->posX+3 == posiX) && (this->posY - 4 == posiY) ||
            (this->posX+3 == posiX) && (this->posY - 3 == posiY) ||
            (this->posX+3 == posiX) && (this->posY - 2 == posiY) ||
            (this->posX+3 == posiX) && (this->posY - 1 == posiY) ||
            (this->posX+3 == posiX) && (this->posY     == posiY) ||
            
            (this->posX - 6 == posiX) && (this->posY-3 == posiY) ||
            (this->posX - 5 == posiX) && (this->posY-3 == posiY) ||
            (this->posX - 4 == posiX) && (this->posY-3 == posiY) ||
            (this->posX - 3 == posiX) && (this->posY-3 == posiY) ||
            (this->posX - 2 == posiX) && (this->posY-3 == posiY) ||
            (this->posX - 1 == posiX) && (this->posY-3 == posiY) ||
            (this->posX     == posiX) && (this->posY-3 == posiY) ||

            (this->posX - 6 == posiX) && (this->posY+3 == posiY) ||
            (this->posX - 5 == posiX) && (this->posY+3 == posiY) ||
            (this->posX - 4 == posiX) && (this->posY+3 == posiY) ||
            (this->posX - 3 == posiX) && (this->posY+3 == posiY) ||
            (this->posX - 2 == posiX) && (this->posY+3 == posiY) ||
            (this->posX - 1 == posiX) && (this->posY+3 == posiY) ||
            (this->posX     == posiX) && (this->posY+3 == posiY) 
        )
        {
            dano = this->attack - enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->get_defense();

            cout << endl << endl;
            cout << "==> "; 
            cout << enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->get_type() << " (" << campo.get_id_army_en_cell(posiX, posiY) << ")";
            cout << " recibió " << dano << " puntos de daño." << endl << endl;

            enemigo[ campo.get_id_army_en_cell(posiX, posiY) ]->set_hitPoints( dano );
            
            return true;
        }
        else return false;
    }
    else return false;
}
