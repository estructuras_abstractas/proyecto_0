/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Field.cpp
 * @brief Se definen los métodos de la clase Field.
 */

#include <iostream>
#include <random> ///< Biblioteca para generar números pseudoaleatorios.
#include "../include/Field.h"

using namespace std;


void Field::iniciar_field ( int rows, int cols, int MAXIMO_NUM_CASILLAS_NO_PASSABLES )
{
    int casillas_no_pasables;
    int fila_aleatoria;
    int columna_aleatoria;

    this->rows = rows;
    this->cols = cols;

    //< Inicializa la matriz doble playfield.
    this->playfield = new Cell* [rows];

    for ( int i=0; i<rows; i++)
    {
        this->playfield[i] = new Cell [cols];
    }


    //< Se determina cuantas casillas no pasables habrá y en que posición.
    casillas_no_pasables = this->numero_aleatorio ( MAXIMO_NUM_CASILLAS_NO_PASSABLES );
    
    for ( int i=0; i < casillas_no_pasables; i++)
    {
        fila_aleatoria    = this->numero_aleatorio ( rows-1 );
        columna_aleatoria = this->numero_aleatorio ( cols-1 );
        playfield [ fila_aleatoria ][ columna_aleatoria ].setPassable (false);
    }
}


bool Field::posicion_playfield_valida ( int posX, int posY )
{
    //< Verifica si la posición dada es pasable y si hay alguien.
    if (  (posY >= 0) && (posY < this->cols) && (posX >= 0) && (posX < this->rows) )
    {
        if ( (this->playfield [posX][posY].Hay_alguien_en_la_celda() == false) && (this->playfield [posX][posY].es_passable() == true) )
        {
            return true;
        }
        
        else return false;
    }

    else return false;
}


void Field::imprimir_estado_field()
{
    cout << endl << endl << " ";

    for ( int i=0; i<this->rows; i++)
        cout << " " << i;

    cout << endl;

    for ( int i=0; i<this->rows; i++ )
    {
        cout << i << " ";

        for ( int j=0; j<this->cols; j++ )
        {
            if ( this->playfield[i][j].Hay_alguien_en_la_celda() )
            {
                cout << this->playfield[i][j].getCellUnitType() << " " ;
            }
            else
            {
                if ( this->playfield[i][j].es_passable() )
                {
                    cout << "- " ;
                }
                else
                {
                    cout << "x ";
                }
                
            }
            
        }

        cout << endl;
    }
}


int Field::numero_aleatorio ( int rangoMax )
{
    /**
     * Genera y devuelve un número pseudoaleatorio.
     */
    random_device generator; 
    mt19937 gen(generator());
    uniform_int_distribution<> distribution(1, rangoMax);

    return distribution(generator);
}


void Field::reset_cell ( int posX, int posY )
{
    this->playfield [posX][posY].reset_celda();
}


bool Field::hay_alguien ( int posX, int posY )
{
    if ( this->playfield [posX][posY].Hay_alguien_en_la_celda() == true )
    {
        return true;
    }
    else return false;
}


int Field::get_rows()
{
    return this->rows;
}


int Field::get_cols()
{
    return this->cols;
}


Cell** Field::get_playfield ()
{
    return this->playfield;
}


int Field::get_id_army_en_cell ( int posX, int posY)
{
    return this->playfield[posX][posY].get_id_army();
}


void Field::set_army ( char army, int id, int posX, int posY )
{
    this->playfield [posX][posY].setArmy ( army, id );
}


void Field::set_passable ( bool pasa, int posX, int posY )
{
    this->playfield [posX][posY].setPassable (pasa);
}
