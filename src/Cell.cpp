/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Cell.cpp
 * @brief Se definen los métodos de la clase Cell.
 */

#include <iostream>
#include "../include/Cell.h"

using namespace std;


Cell::Cell ()
{
    this->passable = true;
    this->army = '-';
}


bool Cell::Hay_alguien_en_la_celda ()
{
    if ( this->army == '-' )
    {
        return false;
    }
    else
    {
        return true;
    }
}


bool Cell::es_passable ()
{
    return this->passable;
}


void Cell::imprimir_estado_cell ()
{
    cout << "-> Franqueable: " << (this->passable == true)? "Si":"No";
    cout << endl << endl;
    cout << "-> Unidad: " << (this->army == '-')? '-': this->army ;
}


void Cell::reset_celda ()
{
    this->army = '-';
}


int Cell::get_id_army()
{
    return this->id;
}


char Cell::getCellUnitType ()
{
    return this->army;
}


void Cell::setPassable( bool passable )
{
    this->passable = passable;
}


bool Cell::setArmy ( char guerrero, int army_id )
{
    if( this->army == '-' && this->passable == true )
    {
        this->army = guerrero;
        this->id = army_id;
        return true;
    }
    else
        return false;
}