/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file main.cpp
 * @brief Lleva la lógica del juego.
 */

#include <iostream>
#include <vector>
#include <string>
#include <stdio_ext.h>

#include "../include/Unit.h"
#include "../include/Cell.h"
#include "../include/Field.h"
#include "../include/Player.h"
#include "../include/UI.h"

#define ERROR_DE_CUAL_JUGADOR_JUEGA 10
#define FINALIZO_JUEGO 0

#define PROGRAMA_TERMINO_CON_EXITO 0
#define MAXIMO_NUM_CASILLAS_NO_PASSABLES 3
#define FILAS 5
#define COLUMNAS 5

#define JUGADOR1 1
#define JUGADOR2 2

#define NUEVA_PARTIDA 1
#define CARGAR 2
#define GUARDAR 3

using namespace std;


int main (void)
{
    int cargar_guardar; //< Un valor de 0 carga, un valor de 1 guarda, un valor de 2 es una nueva partida.
    char guardar;
    bool continuar_partida = false;
    bool bucle_guardar;
    bool escoger_slot;
    int slot;

    Field Campo;
    Player player1;
    Player player2;

    vector<Unit*> army1;
    vector<Unit*> army2;

    Cell** playfield;

    UI ui;

    int turno; 
    int quien_juega; 
    bool primera_carga = false;

    string slot1 = "./PARTIDA_1.txt"; 
    string slot2 = "./PARTIDA_2.txt";
    string slot3 = "./PARTIDA_3.txt";



    while ( continuar_partida == false )
    {
        cin.clear();
        __fpurge(stdin);
        cout << endl;
        cout << endl << " 1. Nueva partida";
        cout << endl << " 2. Cargar partida";
        cout << endl;
        cin >> cargar_guardar;


        if ( cargar_guardar == NUEVA_PARTIDA )
        {
            turno = 1;
            quien_juega = JUGADOR1;
            continuar_partida = true;


            Campo.iniciar_field (FILAS, COLUMNAS, MAXIMO_NUM_CASILLAS_NO_PASSABLES);

            Campo.imprimir_estado_field();
            cout << endl << endl << "*******************************************" << endl;
            cout <<                 "                  Jugador 1                " << endl;
            cout <<                 "*******************************************" << endl << endl; 
            player1.createArmy ( Campo, JUGADOR1 );

            cout << endl << endl << "*******************************************" << endl;
            cout <<                 "                  Jugador 2             " << endl;
            cout <<                 "*******************************************" << endl << endl;    
            player2.createArmy ( Campo, JUGADOR2 );
            Campo.imprimir_estado_field();


            army1 = player1.get_army();
            army2 = player2.get_army();
        }
        
        else if ( cargar_guardar == CARGAR )
        {
            continuar_partida = true;

            cout << endl << endl << "-> Escoja slot a cargar :) ";

            escoger_slot = true;

            while ( escoger_slot == true )
            {
                cin.clear();
                __fpurge(stdin);
                cout << endl;
                cout << endl << " 1. Slot 1 " << endl;
                cout << " 2. Slot 2" << endl;
                cout << " 3. Slot 3" << endl;
                cin >> slot;

                if ( slot == 1 )
                {
                    ui.cargar_juego (
                        slot1, 
                        turno,
                        quien_juega,
                        MAXIMO_NUM_CASILLAS_NO_PASSABLES,
                        Campo,
                        player1,
                        player2,
                        army1,
                        army2
                    );

                    escoger_slot = false;
                }

                else if ( slot == 2 )
                {
                    ui.cargar_juego (
                        slot2, 
                        turno,
                        quien_juega,
                        MAXIMO_NUM_CASILLAS_NO_PASSABLES,
                        Campo,
                        player1,
                        player2,
                        army1,
                        army2
                    );

                    escoger_slot = false;
                }

                else if ( slot == 3 )
                {
                    ui.cargar_juego (
                        slot3, 
                        turno,
                        quien_juega,
                        MAXIMO_NUM_CASILLAS_NO_PASSABLES,
                        Campo,
                        player1,
                        player2,
                        army1,
                        army2
                    );

                    escoger_slot = false;
                }

                else cout << "-> Es fácil, 1 para el primer espacio, 2 para el segundo... -_- " << endl << endl; 
            
            }//Fin de while

            //Campo.imprimir_estado_field(); 
            
            primera_carga = true;
            player1.actualiza_army ( army1 );
            player2.actualiza_army ( army2 ); 
        }
        
        else cout << endl << endl << "-> Esa opción no existe :/" << endl << endl;
    }//Fin de while



    continuar_partida = true;

    while ( continuar_partida == true )
    {
        // Logica para guardar la partida

        if ( primera_carga == false)
        {
            bucle_guardar = true;

            while ( bucle_guardar == true )
            {
                cin.clear();
                __fpurge(stdin);
                cout << endl;
                cout << endl << " ==> ¿Guardar partida? <==  ";
                cin >> guardar;


                if ( guardar == 's')
                {
                    playfield = Campo.get_playfield();
                    //Elige el slot para guardar
                    escoger_slot = true;

                    while ( escoger_slot == true )
                    {
                        cin.clear();
                        __fpurge(stdin);
                        cout << endl;
                        cout << endl << " 1. Slot 1 " << endl;
                        cout << " 2. Slot 2" << endl;
                        cout << " 3. Slot 3" << endl;
                        cin >> slot;

                        if ( slot == 1 )
                        {
                            ui.guardar_juego(
                                slot1, 
                                turno,
                                quien_juega,
                                Campo,
                                playfield,
                                player1,
                                player2,
                                army1,
                                army2
                            );

                            escoger_slot = false;
                        }

                        else if ( slot == 2 )
                        {
                            ui.guardar_juego(
                                slot2, 
                                turno,
                                quien_juega,
                                Campo,
                                playfield,
                                player1,
                                player2,
                                army1,
                                army2
                            );

                            escoger_slot = false;
                        }

                        else if ( slot == 3 )
                        {
                            ui.guardar_juego(
                                slot3, 
                                turno,
                                quien_juega,
                                Campo,
                                playfield,
                                player1,
                                player2,
                                army1,
                                army2
                            );

                            escoger_slot = false;
                        }

                        else cout << "-> Es fácil, 1 para el primer espacio, 2 para el segundo... -_- " << endl << endl;
                    }
                    
                    bucle_guardar = false;
                }

                else if ( guardar == 'n')
                {
                    bucle_guardar = false;
                }

                else cout << endl <<  "-> Escriba s para sí o n para no  >:/" << endl << endl;
                
            }//Fin de while

        }

        primera_carga = false;

        //Termina la logica de guardar partida
                                
        cout << endl << endl << "********* TURNO " << turno << " *********" << endl;


        if ( quien_juega == JUGADOR1 )
        {         
            cout << "---------- " << player1.get_name()  << " ----------" << endl;
            cout << "~score: " << player1.get_score() << endl;

            Campo.imprimir_estado_field();

            player1.imprimir_estado_unidad();
            
            player1.play( Campo, army2 );
            quien_juega = JUGADOR2;

        
            if( army2[0]->get_idcount() == 0 )
            {
                cout << endl << "************************************" << endl;
                cout <<         "         Terminó el juego           " << endl;
                cout <<         "************************************" << endl << endl;
                cout << "     => " << player1.get_name() << " ganó el juego con una puntuación de " << player1.get_score() << " <=" << endl << endl;   
                return FINALIZO_JUEGO;
            }

            player2.actualiza_army( army2 );
        }

        else if ( quien_juega == JUGADOR2 )
        {
            cout << "---------- " << player2.get_name()  << " ----------" << endl;
            cout << "~score: " << player2.get_score() << endl;

            Campo.imprimir_estado_field();

            player2.imprimir_estado_unidad();

            player2.play( Campo, army1 );
            quien_juega = JUGADOR1;

                
            if( army1[0]->get_idcount() == 0 )
            {
                cout << endl << "************************************" << endl;
                cout <<         "         Terminó el juego           " << endl;
                cout <<         "************************************" << endl << endl;
                cout << "     => " << player2.get_name() << " ganó la partida con una puntuación de " << player2.get_score() << " <=" << endl << endl;  
                return FINALIZO_JUEGO;
            }

            player1.actualiza_army( army1 );
        }

        else return ERROR_DE_CUAL_JUGADOR_JUEGA;


        turno++;

    }//Fin de while

    
    army1.clear();
    army2.clear();

    return PROGRAMA_TERMINO_CON_EXITO;
}
