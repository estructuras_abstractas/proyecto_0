/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file UI.cpp
 * @brief Se definen los métodos de la clase UI.
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <bits/stdc++.h> 
#include "../include/Cell.h"
#include "../include/Player.h"
#include "../include/Archer.h"
#include "../include/Lancer.h"
#include "../include/Cavalry.h"
#include "../include/Unit.h"
#include "../include/UI.h"

#define ARCHIVO_NO_EXISTE 0
#define ARCHIVO_EXISTE 1
#define ERROR_CODIFICACION 2

using namespace std;


//Se usa en cargar_juego
void UI::sacar_datos_string (string str, vector<string> &palabra )//El tamano del vector es size+1 
{     
    // Divide el string en base a sus espacios. 
    istringstream ss(str); 
  
    // Revisa todas las palabras 
    do { 
        // Lee la palabra
        string word;
        ss >> word; 
  
        // La mete en el vector
        palabra.push_back (word);
      
        // Mientras haya mas que leer 
    } while (ss); 
} 


int UI::cargar_juego (
    string archivo,
    int &turno,
    int &quien_juega, 
    int casillas_no_pasables,
    Field &campo,
    Player &player1,
    Player &player2,
    vector<Unit*> &army1,
    vector<Unit*> &army2
)
{
    vector<string> datos;
    string linea;

    ifstream partida;
    partida.open( archivo );

    if( !partida ) 
    {
        cout<<endl<<"-> En ese slot no hay ninguna partida guardada :("; 
        return ARCHIVO_NO_EXISTE;
    }
    

    //< Se lee el archivo y se extraen los datos de el.
    while ( !partida.eof() )
    {
        linea.clear();

        getline( partida, linea);

        datos.clear();
        this->sacar_datos_string ( linea, datos );


        if ( datos[0].compare("field") == 0 )
        {
            campo.iniciar_field ( stoi(datos[1]), stoi(datos[2]), casillas_no_pasables );
        }   

        else if ( datos[0].compare("turno") == 0 )
        {
            turno = stoi(datos[1]);
        }

        else if ( datos[0].compare("quien") == 0 )
        {
            quien_juega = stoi(datos[1]);
        }

        else if ( datos[0].compare("cell") == 0)
        {              
            campo.set_army( datos[4][0], stoi(datos[5]), stoi(datos[1]), stoi(datos[2]) );

            campo.set_passable( stoi(datos[3]) , stoi(datos[1]), stoi(datos[2]) );
        }

        else if ( datos[0].compare("j1") == 0 )
        {
            player1.set_maxCost ( stoi(datos[1]) );
            player1.set_name  ( datos[2] );
            player1.set_score ( stoi(datos[3]) );
        }


        else if ( datos[0].compare("army1") == 0 )
        {
            if ( datos[2][0] == 'A' )
            {
                army1.push_back( new Archer() );
            }

            else if ( datos[2][0] == 'L' )
            {
                army1.push_back( new Lancer() );
            }
            
            else if ( datos[2][0] == 'C' )
            {
                army1.push_back( new Cavalry() );
            }

            army1.back()->posicion_inicial  ( stoi(datos[11]), stoi(datos[12]) );
            army1.back()->set_id            ( stoi(datos[1]) );
            army1.back()->set_type          ( datos[2][0] );
            army1.back()->set_maxHitpoints  ( stoi(datos[3]) );
            army1.back()->set_hitPoints     ( stoi(datos[4]), 's' );
            army1.back()->set_attack        ( stoi(datos[5]) );
            army1.back()->set_defense       ( stoi(datos[6]) ); 
            army1.back()->set_range         ( stoi(datos[7]) );
            army1.back()->set_level         ( stoi(datos[8]) );
            army1.back()->set_exp           ( stoi(datos[9]) );
            army1.back()->set_movement      ( stoi(datos[10]) );
            army1.back()->set_cost          ( stoi(datos[13]) );
            army1.back()->set_idCount       ( stoi(datos[14]) );
            army1.back()->set_vive          ( stoi(datos[15]) );
        }



        else if ( datos[0].compare("j2") == 0 )
        {
            player2.set_maxCost ( stoi(datos[1]) );
            player2.set_name  ( datos[2] );
            player2.set_score ( stoi(datos[3]) );
        }


        else if ( datos[0].compare("army2") == 0 )
        {
            if ( datos[2][0] == 'A' )
            {
                army2.push_back( new Archer() );
            }

            else if ( datos[2][0] == 'L' )
            {
                army2.push_back( new Lancer() );
            }
            
            else if ( datos[2][0] == 'C' )
            {
                army2.push_back( new Cavalry() );
            }

            army2.back()->posicion_inicial  ( stoi(datos[11]), stoi(datos[12]) );
            army2.back()->set_id            ( stoi(datos[1]) );
            army2.back()->set_type          ( datos[2][0] );
            army2.back()->set_maxHitpoints  ( stoi(datos[3]) );
            army2.back()->set_hitPoints     ( stoi(datos[4]), 's' );
            army2.back()->set_attack        ( stoi(datos[5]) );
            army2.back()->set_defense       ( stoi(datos[6]) ); 
            army2.back()->set_range         ( stoi(datos[7]) );
            army2.back()->set_level         ( stoi(datos[8]) );
            army2.back()->set_exp           ( stoi(datos[9]) );
            army2.back()->set_movement      ( stoi(datos[10]) );
            army2.back()->set_cost          ( stoi(datos[13]) );
            army2.back()->set_idCount       ( stoi(datos[14]) );
            army2.back()->set_vive          ( stoi(datos[15]) );
        }

        else return ERROR_CODIFICACION;

    }//Fin de while




    partida.close();
    partida.clear();    
    cout << endl << endl << "=> Partida cargada <=" << endl << endl;
}


int UI::guardar_juego ( 
    string archivo,
    int turno,
    int quien_juega, 
    Field campo,
    Cell** cell,
    Player player1,
    Player player2,
    vector<Unit*> army1,
    vector<Unit*> army2
)
{
    ofstream partida;
    partida.open( archivo.c_str(), ofstream::out  | ofstream::trunc ); //<Abre el archivo y elimina su contenido.

    if( !partida ) 
    {
        cout<<endl<<"No se pudo abrir la partida :("; 
        return ARCHIVO_NO_EXISTE;
    }
    
    //Field
    partida << "field " << campo.get_rows() << " " << campo.get_cols() << endl;//< Guarda los datos del campo.

    //turno
    partida << "turno " << turno << endl;

    //quien juega
    partida << "quien " << quien_juega << endl;

    //Cell
    for ( int i=0; i<campo.get_rows(); i++ )
    {
        for ( int j=0; j<campo.get_cols(); j++ )
        {
            partida << "cell " << i << " " << j << " " << cell[i][j].es_passable() << " ";
            partida << cell[i][j].getCellUnitType() << " " << cell[i][j].get_id_army() << endl;
        }//Fin de for
    }//Fin de for


    //Jugador 1
    partida << "j1 " <<  player1.get_maxCost() << " " << player1.get_name() << " " << player1.get_score() << endl;

    //< Armada del jugador 1, guarda los datos del primer jugador.
    for ( int i=0; i<army1.size(); i++ )
    {
        partida << "army1 " << " " << army1[i]->get_id() << " " << army1[i]->get_type() << " " << army1[i]->get_maxHitPoints();
        partida << " " << army1[i]->get_hitPoints() << " " << army1[i]->get_attack() << " " << army1[i]->get_defense();
        partida << " " << army1[i]->get_range() << " " << army1[i]->get_level() << " " << army1[i]->get_experience();
        partida << " " << army1[i]->get_movement() << " " << army1[i]->get_posX() << " " << army1[i]->get_posY();
        partida << " " << army1[i]->get_cost() << " " << army1[i]->get_idcount() << " " << army1[i]->get_vive() << endl;
    }



    //Jugador 2
    partida << "j2 " <<  player2.get_maxCost() << " " << player2.get_name() << " " << player2.get_score() << endl;

    //Armada del jugador 2, guarda los datos del segundo jugador.
    for ( int i=0; i<army2.size(); i++ )
    {
        partida << "army2 " << " " << army2[i]->get_id() << " " << army2[i]->get_type() << " " << army2[i]->get_maxHitPoints();
        partida << " " << army2[i]->get_hitPoints() << " " << army2[i]->get_attack() << " " << army2[i]->get_defense();
        partida << " " << army2[i]->get_range() << " " << army2[i]->get_level() << " " << army2[i]->get_experience();
        partida << " " << army2[i]->get_movement() << " " << army2[i]->get_posX() << " " << army2[i]->get_posY();
        partida << " " << army2[i]->get_cost() << " " << army2[i]->get_idcount() << " " << army2[i]->get_vive() << endl;
    }
    
    partida.close();
    partida.clear();

    cout << endl << endl << "=> Partida guardada <=" << endl << endl;
}
