#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Cavalry.h
 * @brief Archivo cabecera donde se define la estructura de la clase concreta Cavalry.
 */

#include "Unit.h"
#include "Field.h"

/**
 * @class Cavalry
 * @brief Se declaran los métodos y atributos de una clase tipo Cavalry.
 */

class Cavalry : public Unit
{
    public:

        /**
         * @brief Inicializa las características del caballero.
         */
        Cavalry ();

        /**
         * @brief Ataca a un contrincante, devuelve si hubo éxito o no.
         * @param posiX Coordenada X a moverse.
         * @param posiY Coordenada Y a moverse.
         * @param campo El campo de juego.
         * @return Verdadero si la posición es válida.
         */
        bool move ( int posiX, int posiY, Field campo );
};
