#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Lancer.h
 * @brief Archivo cabecera donde se define la estructura de la clase concreta Lancer.
 */

#include "Unit.h"

/**
 * @class Lancer
 * @brief Se declaran los métodos y atributos de una clase tipo Lancer.
 */

class Lancer : public Unit
{
    public:

        /**
         * @brief Inicializa las características del lancer.
         */
        Lancer ();
};
