#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Field.h
 * @brief Archivo cabecera donde se define la estructura de la clase Field.
 */

#include "Cell.h"

/**
 * @class Field
 * @brief Se declaran los métodos y atributos de una clase Field.
 */

class Field
{
    public:

        /**
         * @brief Inicializa el campo de juego y el número de celdas que habrá en el.
         * @param rows Cantidad de filas.
         * @param cols Cantidad de columnas.
         * @param MAXIMO_NUM_CASILLAS_NO_PASSABLES Número de casillas que no podrán ser pasables ni podrán admitir unidades.
         */
        void iniciar_field ( int rows, int cols, int MAXIMO_NUM_CASILLAS_NO_PASSABLES );

        /**
         * @brief Indica si la posición indicada es válida.
         * @param posX Coordenada X de la casilla.
         * @param posY Coordenada Y de la casilla.
         * @return
         */
        bool posicion_playfield_valida ( int posX, int posY );
    
        /**
         * @brief Imprime el contenido del campo.
         */
        void imprimir_estado_field ();

        /**
         * @brief Regresa un número aleatorio entre 1 y rangoMax.
         * @param rangoMax Número máximo que podrá regresar la función.
         * @return Un número aleatorio entre 1 y rangoMax.
         */
        int numero_aleatorio ( int rangoMax );

        /**
         * @brief Pone una casilla del campo en su estado inicial.
         * @param posX Coordenada X de la casilla.
         * @param posY Coordenada Y de la casilla.
         */
        void reset_cell ( int posX, int posY );

        /**
         * @brief Indica si hay alguien en la casilla.
         * @param posX Coordenada X de la casilla.
         * @param posY Coordenada Y de la casilla.
         * @return True si hay una unidad en la casilla.
         */
        bool hay_alguien ( int posX, int posY );


        /**
         * @brief Devuelve el número de filas del campo.
         * @return Número de filas del campo.
         */
        int get_rows();


        /**
         * @brief Devuelve el número de columnas del campo.
         * @return Número de columnas del campo.
         */
        int get_cols();


        /** 
         * @brief Regresa el array doble tipo Cell del que esta compuesto el campo de juego.
         * @return Array doble tipo Cell
         */
        Cell** get_playfield ();


        /**
         * @brief Regresa el id de la unidad que se encuentra en la celda.
         * @param posX Coordenada X de la celda.
         * @param posY Coordenada Y de la celda.
         * @return Id de la unidad en la celda.
         */
        int get_id_army_en_cell ( int posX, int posY);


        /**
         * @brief Coloca una unidad en una celda del campo.
         * @param army Tipo de unidad que se colocará en la celda.
         * @param id Id de la unidad que se colocará en la celda.
         * @param posX Coordenada X de la celda.
         * @param posY Coordenada Y de la celda.
         */
        void set_army ( char army, int id, int posX, int posY );


        /** 
         * @brief Cambia el estado pasable de la celda.
         * @param pasa Si se desea que la celda sea pasable (true) o no (false).
         * @param posX Coordenada X de la celda.
         * @param posY Coordenada Y de la celda.
         */
        void set_passable ( bool pasa, int posX, int posY );
        
        
    private:

        int rows; //< Filas del tablero.

        int cols; //< Columnas del tablero.

        Cell** playfield;//< Tablero.
};
