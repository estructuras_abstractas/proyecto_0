#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 3/02/2020
 * 
 * @file Archer.h
 * @brief Archivo cabecera donde se define la estructura de la clase concreta Archer.
 */

#include "Unit.h"

/**
 * @class Archer
 * @brief Se declaran los métodos y atributos de una clase tipo Archer.
 */

class Archer : public Unit
{
    public:

        /**
         * @brief Inicializa las características del arquero.
         */
        Archer ();

        /**
         * @brief Ataca a un contrincante, devuelve si hubo éxito o no, el ataque se hace en un cuadrado de 6x6.
         * @param posiX Coordenada X a atacar.
         * @param posiY Coordenada Y a atacar.
         * @param enemigo Vector con las unidades del otro jugador.
         * @param campo El campo de juego.
         * @return True si el ataque se completó.
         */
        bool atacar ( int posiX, int posiY, std::vector<Unit*> &enemigo, Field campo );
};