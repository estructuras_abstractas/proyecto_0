#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Player.h
 * @brief Archivo cabecera donde se define la estructura de la clase Player.
 */

#include <string>
#include <vector>

#include "Unit.h"
#include "Field.h"


/**
 * @class Player
 * @brief Se declaran los métodos y atributos de una clase tipo Player.
 */

class Player
{
    public:

        /**
         * @brief Constructor, inicializa los atributos maxCost y score.
         */
        Player ();

        /**
         * @brief Destructor, limpia el vector army.
         */
        ~Player();

        /**
         * @brief Coloca a las unidades cuando son invocadas.
         * @param campo Campo de juego donde se colocarán las unidades.
         * @param jugador A cual jugador le corresponden las unidades.
         * @param idcount Id de las unidades que se colocarán.
         */
        void posicion_inicial ( Field campo, int jugador, int idcount );

        /**
         * @brief Crea la armada del jugador y también solicita el nombre del jugador.
         * @param campo Campo de juego.
         * @param jugador A cual jugador le pertenece la armada.
         */
        void createArmy ( Field campo, int jugador );

        /** 
         * @brief Lógica de un turno del jugador.
         * @param campo Campo de juego donde se llevará a cabo la partida.
         * @param enemigo Vector de enemigos del contrincante.
         */
        void play ( Field &campo, std::vector<Unit*> &enemigo );

        /**
         * @brief Asigna la nueva armada al jugador.
         * @param nueva_army Nueva armada que se asignará.
         */
        void actualiza_army( std::vector<Unit*> nueva_army );


        //Regresa los parámetros de player

        /**
         * @brief Devuelve el dinero del que dispone el jugador.
         * @return Dinero que tiene el jugador.
         */
        int get_maxCost ();

        /**
         * @brief Devuelve el vector que contiene todas las unidades del jugador.
         * @return Vector que contiene las unidades del jugador.
         */
        std::vector<Unit*> get_army();

        /**
         * @brief Devuelve el nombre del jugador.
         * @return Nombre del jugador.
         */
        std::string get_name();

        /**
         * @brief Devuelve la puntuación del jugador.
         * @return Puntuación del jugador.
         */
        int get_score();


        /**
         * @brief Cambia el valor del dinero que tiene el jugador.
         * @param maxcost Dinero que tendrá el jugador.
         */
        void set_maxCost ( int maxcost );

        /**
         * @brief Asigna el nombre del jugador.
         * @param nombre Nombre del jugador.
         */
        void set_name ( std::string nombre );

        /**
         * @brief Asigna la puntuación del jugador.
         * @param puntuacion Puntuación del jugador.
         */
        void set_score ( int puntuacion );

        /**
         * @brief Despliega el estado de la unidad en pantalla.
         * @param id Número de id de la unidad.
         */
        void imprimir_estado_unidad ();


    private:

        int maxCost;//< Dinero máximo que tiene el jugador.
        std::vector<Unit*> army; //Arreglo de tipo unit que contiene todas las unidades del jugador
        std::string name;//< Nombre del jugador
        int score;//< Puntuación del jugador.
};
