#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file UI.h
 * @brief Archivo cabecera donde se define la estructura de la clase UI.
 */

#include <string>
#include <vector>
#include "Field.h"

/**
 * @class UI
 * @brief Se declaran los métodos y atributos de una clase tipo UI.
 */

class UI
{
    public:

        /**
         * @brief Extrae todas las palabras de un string y las coloca en un vector.
         * @param str Palabra de la cual se extraerán las palabras.
         * @param palabra Vector que contendrá las palabras.
         */
        void sacar_datos_string (std::string str, std::vector<std::string> &palabra );


        /**
         * @brief Carga todos los atributos que se encuentran almacenados en un archivo de texto.
         * @param archivo Dirección del archivo de texto.
         * @param turno Turno actual del juego.
         * @param quien_juega Siguiente jugador del turno.
         * @param casillas_no_pasables Cuantas casillas no son pasables.
         * @param campo Clase tipo Field que contiene el campo de juego.
         * @param player1 Clase tipo Player que contiene al jugador 1 y todos sus parámetros.
         * @param player2 Clase tipo Player que contiene al jugador 2 y todos sus parámetros.
         * @param army1 Vector que contiene todas las unidades del jugador 1.
         * @param army2 Vector que contiene todas las unidades del jugador 2.
         * @return Si hubo o no éxito.
         */
        int cargar_juego (
            std::string archivo,
            int &turno,
            int &quien_juega, 
            int casillas_no_pasables,
            Field &campo,
            Player &player1,
            Player &player2,
            std::vector<Unit*> &army1,
            std::vector<Unit*> &army2   
        );


        /**
         * @brief Guarda todos los atributos que se encuentran almacenados en un archivo de texto.
         * @param archivo Dirección del archivo de texto.
         * @param turno Turno actual del juego.
         * @param quien_juega Siguiente jugador del turno.
         * @param campo Clase tipo Field que contiene el campo de juego.
         * @param cell Arreglo doble con todas las celdas del campo.
         * @param player1 Clase tipo Player que contiene al jugador 1 y todos sus parámetros.
         * @param player2 Clase tipo Player que contiene al jugador 2 y todos sus parámetros.
         * @param army1 Vector que contiene todas las unidades del jugador 1.
         * @param army2 Vector que contiene todas las unidades del jugador 2.
         * @return Si hubo o no éxito.
         */
        int guardar_juego ( 
            std::string archivo, //<Dirección del archivo a usar.
            int turno, //<Número de turno del jugador
            int quien_juega, //<Cual es el jugador que sigue.
            Field campo, //<Campo actual.
            Cell** cell,
            Player player1,
            Player player2,
            std::vector<Unit*> army1,
            std::vector<Unit*> army2
        );
};
