#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Cell.h
 * @brief Contiene la plantilla de una clase tipo Cell.
 */


/**
 * @class Cell
 * @brief Se declaran los métodos y atributos de una clase tipo Cell.
 */

class Cell
{
    public:

        /**
         * @brief Constructor, inicia inicia las variables de passable y de army (inicia en -).
         */
        Cell ();

        /**
         * @brief Indica si la celda hay un personaje.
         * @return Verdadero en caso de que si esté ocupada.
         */
        bool Hay_alguien_en_la_celda ();

        /**
         * @brief Indica si la celda es pasable.
         * @return Verdadero en caso de que si sea pasable.
         */
        bool es_passable ();

        /**
         * @brief Despliega en pantalla en estado actual del campo de juego.
         */
        void imprimir_estado_cell ();

        /**
         * @brief Vacia la celda.
         */
        void reset_celda ();

        /**
         * @brief Obtiene el número identificador de la unidad que está en la celda.
         * @return El id de la unidad que está en la celda.
         */
        int get_id_army();

        /**
         * @brief Regresa el tipo de unidad que hay en la celda.
         * @return A para arquero, L para lancer y C para caballero.
         */
        char getCellUnitType ();


        /**
         * @brief Cambia el estado pasable de la celda.
         * @param passable Si se quiere que la celda sea pasable o no.
         */
        void setPassable( bool passable );


        /**
         * @brief Coloca una unidad en la celda.
         * @param guerrero El tipo de unidad que estará en la celda.
         * @param army_id Número identificador de la unidad que estará en la celda.
         * @return Verdadero en caso de que si haya sido posible colocar la unidad en la celda.
         */
        bool setArmy ( char guerrero, int army_id );



    private:

        bool passable;//< Indica si la celda es franqueable o no.

        char army;//< Indica si hay un personaje en la celda.

        int id;//< Número de unidad.
};

