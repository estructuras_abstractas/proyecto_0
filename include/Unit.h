#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 28/01/2020
 *
 * @file Unit.h
 * @brief Archivo cabecera donde se define la estructura de la clase Unit.
 */

#include <vector>
#include "../include/Field.h"

/**
 * @class Unit
 * @brief Se declaran los métodos y atributos de una clase tipo Unit.
 */

class Unit 
{
    public:

        /**
         * @brief Cambia las coordenadas del jugador.
         * @param posiX Nueva coordenada X.
         * @param posiY Nueva coordenada Y.
         */
        void posicion_inicial ( int posiX, int posiY );

        /**
         * @brief Función virtual, mueve a la unidad por el campo de juego en forma de cruz.
         * @param posX Coordenada X a la que se moverá la unidad.
         * @param posY Coordenada Y a la que se moverá la unidad.
         * @param campo Campo de juego donde se moverá la unidad.
         * @return Verdadero si se pudo mover.
         */
        virtual bool move ( int posX, int posY, Field campo );

        /**
         * @brief Función virtual, ataca a un enemigo, el ataque lo realiza en forma de cruz.
         * @param posiX Coordenada X a la que se atacará.
         * @param posiY Coordenada Y a la que se atacará.
         * @param enemigo Vector que contiene a todas las unidades del enemigo.
         * @param campo Campo del juego.
         * @return Verdadero si se pudo atacar la posición.
         */
        virtual bool atacar ( int posiX, int posiY, std::vector<Unit*> &enemigo, Field campo );

        /**
         * @brief Muestra el estado de la unidad.
         */
        void imprimir_estado_unit ();

        /**
         * @brief Sube la experiencia de la unidad.
         */
        void subir_exp();

        /**
         * @brief La unidad sube un nivel.
         */
        void subir_nivel();



        /**
         * @brief Regresa el id de la unidad.
         * @return Id de la unidad.
         */
        int get_id();

        /**
         * @brief Regresa el tipo de unidad del que se trata.
         * @return Tipo de unidad: L, C, A.
         */
        char get_type();

        /**
         * @brief Regresa la cantidad maxima de vida de la unidad.
         * @return Vida máxima de la unidad.
         */
        int get_maxHitPoints();

        /**
         * @brief Regresa la cantidad de vida actual de la unidad.
         * @return Cantidad de vida actual de la unidad.
         */
        int get_hitPoints();

        /**
         * @brief Regresa el poder de ataque de la unidad.
         * @return El poder de ataque de la unidad.
         */
        int get_attack();

        /**
         * @brief Regresa la cantidad de defensa que tiene la unidad.
         * @return Cantidad de defensa de la unidad.
         */
        int get_defense();  

        /**
         * @brief Regresa el rango de ataque de la unidad.
         * @return Rango de ataque de la unidad.
         */
        int get_range();

        /**
         * @brief Regresa el nivel actual de la unidad.
         * @return Nivel actual de la unidad.
         */
        int get_level();

        /**
         * @brief Regresa la cantidad de experiencia actual de la unidad.
         * @return Experiencia actual de la unidad.
         */
        int get_experience();

        /**
         * @brief Regresa el rango de movimiento de la unidad.
         * @return Rango de movimiento de la unidad.
         */
        int get_movement();

        /**
         * @brief Regresa la posición X de la unidad.
         * @return Posición X de la unidad.
         */
        int get_posX();

        /**
         * @brief Regresa la posición Y de la unidad.
         * @return Posición Y de la unidad.
         */
        int get_posY();

        /**
         * @brief Regresa el precio de la unidad.
         * @return Precio de la unidad.
         */
        int get_cost();

        /**
         * @brief Regresa el número de unidades vivas del jugador.
         * @return Número de unidades vivas del jugador.
         */
        int get_idcount();

        /**
         * @brief Regresa si la unidad está vivo o no.
         * @return Si la unidad está viva o no.
         */
        bool get_vive ();



        /**
         * @brief Actualiza el atributo id de la unidad.
         * @param id Id de la unidad.
         */
        void set_id ( int id );

        /**
         * @brief Actualiza el atributo type de la unidad.
         * @param tipo El tipo de unidad.
         */
        void set_type ( char tipo );

        /**
         * @brief Actualiza el atributo maxHitpoints de la unidad.
         * @param max Cantidad de vida máxima de la unidad.
         */
        void set_maxHitpoints ( int max );

        /**
         * @brief Actualiza el atributo hitPoints de la unidad.
         * @param quitar_vida La cantidad de vida que se le restará a la unidad.
         */
        void set_hitPoints ( int quitar_vida );

        /**
         * @brief Actualiza el atributo hitPoints de la unidad.
         * @param hp La cantidad de vida que tendrá la unidad.
         * @param ignorar Cualquier char.
         */
        void set_hitPoints ( int hp, char ignorar );

        /**
         * @brief Actualiza el atributo attack de la unidad.
         * @param ataque Cantidad de ataque que tendrá la unidad.
         */
        void set_attack ( int ataque );

        /**
         * @brief Actualiza el atributo defense de la unidad.
         * @param defensa Cantidad de defensa que tendrá la unidad.
         */
        void set_defense ( int defensa );

        /**
         * @brief Actualiza el atributo range de la unidad.
         * @param rango Rango de ataque de la unidad.
         */
        void set_range ( int rango );

        /**
         * @brief Actualiza el atributo level de la unidad.
         * @param nivel Nivel que tendrá la unidad.
         */
        void set_level ( int nivel );

        /**
         * @brief Actualiza el atributo experience.
         * @param exp Experiencia que tendrá la unidad.
         */
        void set_exp ( int exp );

        /**
         * @brief Actualiza el atributo movement de la unidad.
         * @param movimiento Rango de movimiento de la unidad.
         */
        void set_movement ( int movimiento );

        /**
         * @brief Actualiza el atributo cost de la unidad.
         * @param costo Costo de la unidad.
         */
        void set_cost ( int costo );

        /**
         * @brief Actualiza el atributo idCount de la unidad.
         * @param idcount Número de unidades vivas del jugador.
         */
        void set_idCount ( int idcount );

        /**
         * @brief Actualiza el atributo vive de la unidad.
         * @param vivo Si la unidad está viva o no.
         */
        void set_vive ( bool vivo );


    protected:

        int id;//< Número de unidad.
        char type;//< L para lancer, C para caballero y A para arquero.
        int maxHitPoints;
        int hitPoints;
        int attack;
        int defense;
        int range;
        int level;
        int experience;
        int movement;
        int posX;
        int posY;
        int cost;
        int idCount; //< Máxima cantidad de unidades que tiene el jugador.

        bool vive; //< Indica si el personaje continua vivo.
};
