ALL: packages comp doxygen

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 1s

	sudo apt install g++ -y
	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y

	@echo " "
	@echo "-> Paquetes instalados"


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p bin build docs

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Cell.o -c ./src/Cell.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Unit.o -c ./src/Unit.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Field.o -c ./src/Field.cpp 

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Archer.o -c ./src/Archer.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Cavalry.o -c ./src/Cavalry.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Lancer.o -c ./src/Lancer.cpp 
	
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Player.o -c ./src/Player.cpp 

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/UI.o -c ./src/UI.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/main.o -c ./src/main.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./bin/proyecto0 ./build/Cell.o ./build/Unit.o ./build/Field.o ./build/main.o ./build/Player.o ./build/Archer.o ./build/Cavalry.o ./build/Lancer.o ./build/UI.o

	@echo " "
	@echo "-> Ejecutable generado"

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

	@echo " "
	@echo "-> Archivo refman.pdf generado  "